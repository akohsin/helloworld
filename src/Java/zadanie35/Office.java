package Java.zadanie35;
//       3. Stwórz klasę Office która reprezentuje pojedyncze biuro, w którym będą obsługiwani klienci.
//        - w klasie office dodaj metodę handle która obsługuje klienta. Parametrem tej metody będzie klient.
//        - w metodzie ma się pojawiać komunikat w formacie: "Klient pesel: {pesel} załatwia sprawę {typ_sprawy}"\

public class Office {
    public void handleCase(Client klient){
        System.out.println("klient pesel: "+klient.getPesel()+" załatwia sprawe "+klient.getSprawa());
    }
}
