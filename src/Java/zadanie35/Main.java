package Java.zadanie35;
//Zadanie 35:
//        TO ZADANIE WYDAJE SIĘ BYĆ TRUDNE, ALE NIE JEST. CHODZI W NIM WYŁĄCZNIE O PODKREŚLENIE RELACJI MIĘDZY OBIEKTAMI.
//        Stwórz system obsługi. Zakładamy istnienie urzędu w którym znajdują się pokoje, w których można załatwić naszą
//        sprawę. Klient powinien mieć do dyspozycji linię poleceń, która przyjmuje ich rejestracje.
//
//      1. Zaczniemy od stworzenia prostego enuma, który będzie reprezentować typ sprawy którą klient chce załatwić w
//        urzędzie. Dostępne typy to REGISTER (rejestracja), UNREGISTER(wyrejestrowanie), CHECK_STATUS(stan klienta)
//
//       2. Stwórzmy klasę Client. Każdy klient posiada typ sprawy który musi podać oraz swój pesel, dlatego:
//        - stwórz konstruktor w klasie klient, która jako parametr przyjmuje typ sprawy oraz pesel i ustawia wartości
//        tych pól w klasie.
//        - stwórz gettery oraz settery tych pól.
//
//       3. Stwórz klasę Office która reprezentuje pojedyncze biuro, w którym będą obsługiwani klienci.
//        - w klasie office dodaj metodę handle która obsługuje klienta. Parametrem tej metody będzie klient.
//        - w metodzie ma się pojawiać komunikat w formacie: "Klient pesel: {pesel} załatwia sprawę {typ_sprawy}"
//
//       4. Stwórz klasę Department która posiada kolekcję (możesz użyć listy lub mapy) biur.
//        - dodaj metodę addOffice która jako parametr przyjmuje biuro.
//        - dodaj metodę getOffice która służy do pobierania biura. Jeśli użyłeś/łaś listy to parametrem tej metody
//        powinien być indeks, natomiast jeśli użyto mapy, to parametrem powinien być typ klucza tej mapy.
//
//       5. Stwórz maina. W mainie stwórz department, dodaj do niego dwa biura. Zweryfikuj działanie metod dodania i
//        pobrania biura. Następnie stwórz obsługiwanie tej funkcjonalności z linii poleceń/konsoli. W konsoli powinniśmy
//        mieć możliwość wpisania "dodaj_biuro" które spowoduje dodanie nowego biura.
//        -- Jeśli użyto mapy, to zaleca się dopisanie nazwy po "dodaj_biuro" która będzie kluczem, po którym będziemy
//        odnajdować biura. Zamysł jest taki, aby - jeśli używamy mapy, to każde biuro ma swoją nazwę i jest po tej nazwie
//        dodawane, a następnie wyszukiwane.
//
//       6. Dodaj w mainie czytanie ze scannera nowego klienta:
//        obsluga {PESEL} {TYP_SPRAWY} {ID_POKOJU}
//        która tworzy nowego klienta, a następnie wykorzystuje klasę department aby pobrać pokój identyfikowany przez
//        ID_POKOJU i obsługuje go metodą handle.

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Department department = new Department();

        department.addOffice(new Office());
        department.addOffice(new Office());
        Scanner sc = new Scanner(System.in);

        System.out.println("dodaj_biuro lub idź dalej");
        String line = sc.nextLine();
        if (line.equals("dodaj_biuro")) department.addOffice(new Office());

        boolean isRunning = true;
        do {
            System.out.println("komenda: wejdź lub wyjdź");
            String komenda = sc.nextLine();
            long pesel;
            switch (komenda) {
                case "wejdź": {
                    try {
                        try {
                            System.out.println("podaj pesel klienta");
                            pesel = Long.parseLong(sc.nextLine().trim());
                        } catch (IllegalArgumentException iae) {
                            throw new PeselNumberException();
                        }
                        System.out.println("podaj typ sprawy (REGISTER, UNREGISTER, CHECK_STATUS");
                        Case sprawa = Case.valueOf(sc.nextLine().trim());
                        System.out.println("podaj numer pokoju");
                        int numer = Integer.parseInt(sc.nextLine().trim()) - 1;
                        if (numer >= department.sizeOfOffice()){
                            throw new NumberOfOfficeException();
                        }
                        department.getOffice(numer).handleCase(new Client(sprawa, pesel));


                    } catch (IllegalArgumentException iae) {
                        System.out.println("wpisz poprawny pesel i sprawę");

                    } catch (NumberOfOfficeException nooe) {
                        System.out.println("nie ma takiego pokoju");
                    }catch (PeselNumberException pse){
                        System.out.println("zły pesel");
                    }
                    break;
                }
                case "wyjdź": {
                    isRunning = false;
                }

            }
        }
        while (isRunning);


    }
}
