package Java.zadanie35;
//       2. Stwórzmy klasę Client. Każdy klient posiada typ sprawy który musi podać oraz swój pesel, dlatego:
//        - stwórz konstruktor w klasie klient, która jako parametr przyjmuje typ sprawy oraz pesel i ustawia wartości
//        tych pól w klasie.
//        - stwórz gettery oraz settery tych pól.

public class Client {
    private Case sprawa;
    private long pesel;

    public Client(Case sprawa, long pesel) {
        this.sprawa = sprawa;
        this.pesel = pesel;
    }

    public Case getSprawa() {
        return sprawa;
    }

    public void setSprawa(Case sprawa) {
        this.sprawa = sprawa;
    }

    public long getPesel() {
        return pesel;
    }

    public void setPesel(long pesel) {
        this.pesel = pesel;
    }
}
