package Java.zadanie35;
//       4. Stwórz klasę Department która posiada kolekcję (możesz użyć listy lub mapy) biur.
//        - dodaj metodę addOffice która jako parametr przyjmuje biuro.
//        - dodaj metodę getOffice która służy do pobierania biura. Jeśli użyłeś/łaś listy to parametrem tej metody
//        powinien być indeks, natomiast jeśli użyto mapy, to parametrem powinien być typ klucza tej mapy.

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Department {
    private List<Office> department = new ArrayList<>();
    public void addOffice(Office biuro){
        department.add(biuro);
    }
    public Office getOffice(int i){
        return department.get(i);
    }
    public int sizeOfOffice(){
        return department.size();
    }



}
