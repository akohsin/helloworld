package Java.zadanie1;

public class MainOsoba {
    public static void main(String[] args) {
        Osoba zmiennaOsoba = new Osoba("Paweł Gaweł", 20);

        // a )
        zmiennaOsoba.printYourNameAndAge();

        // b ) (metoda toString wywoluje sie sama)
        System.out.println(zmiennaOsoba);

        // c )
        System.out.println(zmiennaOsoba.getName() + " " + zmiennaOsoba.getAge());
    }
}
