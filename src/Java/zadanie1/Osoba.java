package Java.zadanie1;

public class Osoba {
    private String name;
    private int age;

    public Osoba(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void SetAge(int age) {
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    public void printYourNameAndAge() {
        System.out.println("name: " + name + " age: " + age);
    }

    @Override
    public String toString() {
        return "Osoba{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';


    }
}
