package Java.zadanie1;

public class Main {
    public static void main(String[] args) {

        Person person1 = new Person("Janek", "Wisniewski", 21);
        Person person2 = new Person("a", "b", 1);

        person2.printOutYourName();

        System.out.println(person1);
//        person1.printOutYourName();

        // 2 instrukcje
//        String nameAndSurname = person1.giveMeYourNameAndSurname();
//        System.out.println(nameAndSurname);

        // 1 instrukcja
//        System.out.println(person1.giveMeYourNameAndSurname());

    }


    //    protected - jest widoczne w package'u oraz w klasach dziedziczących
//    public - jest widoczne wszędzie z zewnątrz
//    private - cokolwiek zadeklarowane private jest widoczne wyłącznie w obrębie klasy
}

