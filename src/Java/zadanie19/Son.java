package Java.zadanie19;

public class Son extends FamilyMember {
    @Override
    public void introduce() {
        System.out.println("I'm a son");
        super.introduce();
    }
    @Override
    public boolean isAdult() {
        return false;
    }
}
