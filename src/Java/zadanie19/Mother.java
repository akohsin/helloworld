package Java.zadanie19;

public class Mother extends FamilyMember{
    @Override
    public void introduce() {
        System.out.println("I'm a mother....");
        super.introduce();
    }
    @Override
    public boolean isAdult() {
        return true;
    }
}
