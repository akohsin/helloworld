package Java.zadanie19;

public class Father extends FamilyMember {
    @Override
    public void introduce() {
        System.out.println("I'm a father");
        super.introduce();
    }

    @Override
    public boolean isAdult() {
        return true;
    }
}
