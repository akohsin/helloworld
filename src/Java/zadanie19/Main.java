package Java.zadanie19;

import java.util.ArrayList;

//Zadanie 19:
//        Utwórz klasę FamilyMember z polem name i metodą introduce() która wypisuje komunikat
//        "I am just a simple family member".
//        Wykorzystaj dziedziczenie (extends FamilyMember ) w klasach Mother, Father, Son, Daugther.
//        W metodzie introduce poszczególnych klas wpisz implementację:
//        - i am mother ...
//        - i am father ...
//        - i am son ...
//        - i am daughter ...
//
//        Umieść rodzinę w liście i za pomocą pętli foreach wywołaj metodę introduce().
//
//        B)
//        Dodaj metodę (*abstrakcyjną) boolean isAdult do klasy FamilyMember,
//        załóż że rodzice są zawsze dorośli, a dzieci nie.
//        Nadpisz te metody w klasach podrzędnych.
public class Main {
    public static void main(String[] args) {
        ArrayList<FamilyMember> rodzina = new ArrayList<>();
        Mother mama = new Mother();
        Father tata = new Father();
        Son syn = new Son();
        rodzina.add(mama);
        rodzina.add(tata);
        rodzina.add(syn);
        for (FamilyMember osoba:rodzina)
            {
                osoba.introduce();

        }
    }
}
