package Java.zadanie21;

//Zadanie 21:
//        Stwórz klasę Citizen oraz klasy dziedziczące
//        Peasant(Chłop),
//        Townsman(Mieszczanin),
//        King(Król),
//        Soldier(Żołnierz)

//        Wszystkie klasy posiadają pole imie
//        Citizen powinien być klasą abstrakcyjną która posiada metodę abstrakcyjną 'canVote' która zwraca true dla
//        townsman'a i soldier'a, ale false dla chłopa i króla.
//        Stwórz klasę Town która posiada listę Citizenów. Dodaj do niej kilku citizenów (różnych w mainie) i
//        stwórz metody howManyCanVote które zwracają ilość osób które mogą głosować.
//        Stwórz w klasie Town metodę "whoCanVote" która zwraca imiona osób które mogą głosować.

import java.util.List;

public class Main {
    public static void main(String[] args) {
        Citizen osoba1 = new King("Julian");
        Citizen osoba2 = new Peasant("Jarek");
        Citizen osoba3 = new Townsman("Darek");
        Citizen osoba4 = new Soldier("Bruce Lee");
        Town miasteczko = new Town();
        miasteczko.dodajOsobe(osoba1);
        miasteczko.dodajOsobe(osoba2);
        miasteczko.dodajOsobe(osoba3);
        miasteczko.dodajOsobe(osoba4);
        List<String> lista = miasteczko.whoCanVote();
        System.out.println(lista);
        System.out.println("ilość osób które mogą głosować to: "+ miasteczko.licznik);
    }
}
