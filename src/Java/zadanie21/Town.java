package Java.zadanie21;

//Zadanie 21:
//        Stwórz klasę Citizen oraz klasy dziedziczące
//        Peasant(Chłop),
//        Townsman(Mieszczanin),
//        King(Król),
//        Soldier(Żołnierz)

//        Wszystkie klasy posiadają pole imie
//        Citizen powinien być klasą abstrakcyjną która posiada metodę abstrakcyjną 'canVote' która zwraca true dla
//        townsman'a i soldier'a, ale false dla chłopa i króla.
//        Stwórz klasę Town która posiada listę Citizenów. Dodaj do niej kilku citizenów (różnych w mainie) i
//        stwórz metody howManyCanVote które zwracają ilość osób które mogą głosować.
//        Stwórz w klasie Town metodę "whoCanVote" która zwraca imiona osób które mogą głosować.

import java.util.ArrayList;
import java.util.List;

public class Town {
    private List<Citizen> spisLudnosci = new ArrayList<>();
    protected int licznik = 0;

    public void dodajOsobe(Citizen osoba) {
        spisLudnosci.add(osoba);
    }

    public List<String> whoCanVote() {
        List<String> listaImion = new ArrayList<>();

        for (Citizen x : spisLudnosci) {
            if (x.canVote()) {
                listaImion.add(x.name);
                licznik++;
            }
        }
        return listaImion;


    }


}
