package Java.zadanie10;

//Zadanie 10*:
//        Do stworzenia jest aplikacja, w której stwórz klasę "Car" reprezentującą samochód.
//        a)
//        Samochód ma pola:
//        - nazwę/markę,
//        - ilość siedzeń,
//        - pojemność silnika,
//        - ilość koni mechanicznych,
//        - obecną prędkość,
//        - obecny przebieg,
//        - rok produkcji
//
//        Samochód ma metody:
//        b)
//        - getCarName - która ZWRACA nazwę/markę samochodu oraz obecny rok produkcji
//        c)
//        - addPassenger - która dodaje pasażera
//        - removePassenger - która odejmuje pasażera
//        d)
//        - speedIncrease - która zwięsza obecną prędkość samochodu
//        - speedDecrease - która zmniejsza prędkość obecną samochodu
//        (samochód może przyspieszać lub zwalniać tylko jeśli jest minimum 1 pasażer[zakładamy że to kierowca])
//        d)
//        Zmodyfikuj samochód tak, aby mógł osiągać maksymalną prędkość nie wyższą niż (1.2*ilość koni mechanicznych)
//        jeśli ma do 200 koni mechanicznych,
//        oraz nie wyższą niż (1.0*ilość koni mechanicznych) jeśli ilość koni przekracza 200.
public class Car {
    private String model;
    private int seats;
    private double engine;
    private int power;
    private int speed;
    private int milage;
    private int production;
    private int passengers;

    public Car(String model, int seats, double engine, int power, int speed, int milage, int production) {
        this.model = model;
        this.seats = seats;
        this.engine = engine;
        this.power = power;
        this.production = production;
        this.milage = milage;
        this.speed = speed;

    }

    public String getCarName() {
        String name = model + production;
        return name;
    }

    public void addPassenger() {
        passengers++;
    }

    public void removePassenger() {
        passengers--;
    }

    public void speedIncrease(int value) {
        if (passengers > 0) {
            if (power < 200) {
                if ((speed += value) < 1.2 * power) speed += value;
                else System.out.println("too fast");
            } else {
                if ((speed += value) < 1 * power) speed += value;
                else System.out.println("too fast");
            }

        } else System.out.println("car is empty");
    }

    public void speedDecrease(int value) {
        if (passengers > 0) {
            speed -= value;
        } else System.out.println("car is empty");
    }


}
