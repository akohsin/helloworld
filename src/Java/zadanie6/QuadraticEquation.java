package Java.zadanie6;

//Napisz klasę QuadraticEquation (obliczanie równania kwadratowego) o trzech polach typu double a,b,c
// odpowiednim konstruktorze i metodach:
//        - public double calculateDelta(),
//        - public double calculateX1(),
//        - public double calculateX2().
public class QuadraticEquation {
    private int a = 0;
    private int b = 0;
    private int c = 0;
    private double delta = 0;
    private double x1 = 0;
    private double x2 = 0;


    public double calculateDelta(int a, int b, int c) {
        delta = b * b - 4 * a * c;
        return delta;
    }

    public double calculateX1(int a, int b, int c) {
        delta = b * b - 4 * a * c;
        if (delta < 0) {
            System.out.println("brak rozwiazan");
            return 0;
        } else {
            if (delta == 0) System.out.println("Uwaga: jedno rozwiązanie równanie");
            x1 = -b - Math.sqrt(delta) / 2 / a;
            return x1;
        }

    }

    public double calculateX2(int a, int b, int c) {
        delta = b * b - 4 * a * c;
        if (delta < 0) {
            System.out.println("brak rozwiazan");
            return 0;
        } else {
            if (delta == 0) System.out.println("Uwaga: jedno rozwiązanie równanie");
            x2 = -b + Math.sqrt(delta) / 2 / a;
            return x2;
        }
    }
}
