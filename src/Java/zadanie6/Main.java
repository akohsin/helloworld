package Java.zadanie6;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        QuadraticEquation equation = new QuadraticEquation();
        Scanner scan = new Scanner(System.in);
        String inputLine;
        int a=0;
        int b=0;
        int c=0;

        do {
            System.out.println("give 3 parameters:");
            inputLine = scan.nextLine();
            String words[] = inputLine.split(" ");

            try {
                a = Integer.parseInt(words[0]);
                b = Integer.parseInt(words[1]);
                c = Integer.parseInt(words[2]);
                System.out.println("delta equals: "+ equation.calculateDelta(a,b,c));
                System.out.println("X1 equals: "+ equation.calculateX1(a,b,c));
                System.out.println("X2 equals: "+ equation.calculateX2(a,b,c));

            } catch (NumberFormatException nfe) {
                System.out.println("wrong format");
            } catch (ArrayIndexOutOfBoundsException aioobe) {
                System.out.println("too less parameters");
            }


        }
        while (!inputLine.equals("quit"));

    }
}
