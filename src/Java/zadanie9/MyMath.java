package Java.zadanie9;

//Zadanie 9:
//        Napisz klasę MyMath zawierającą poniższe metody dla typów int, double:
//        - obliczanie wartości bezwzględnej z liczby: int abs(int a), double abs(double a)
//        - obliczanie potęgi liczby: int pow(int a,int b), double pow(double a,int b)


public class MyMath {
    private double wynik2 = 0;
    private int wynik1 = 0;

    public double abs(double a) {
        if (a < 0) return -a;
        else return a;
    }

    public int abs(int a) {
        if (a < 0) wynik1 = -a;
        else wynik1 = a;
        return wynik1;
    }

    public int pow(int a, int b) {
        for (int i = 0; i <= b; i++) {
            if (i == 0) wynik1 = 1;
            else wynik1 *= a;
        }
        return wynik1;
    }

    public double pow(double a, int b) {
        for (int i = 0; i <= b; i++) {
            if (i == 0) wynik2 = 1;
            else wynik2 *= a;
        }
        return wynik2;
    }

}
