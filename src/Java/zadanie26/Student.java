package Java.zadanie26;
//Zadanie 26:
//        a. Utwór klasę Student o polach: (long) numerIndeksu, imie, nazwisko oraz:
//        b. Utwórz kilku studentów i dodaj do mapy (HashMap).
//        c. sprawdź, czy mapa zawiera studenta o indeksie 100200,
//        d. wypisz studenta o indeksie 100400
//        e. wypisz liczbę studentów
//        f. wypisz wszystkich studentów
//        g. zmień implementacje na TreeMap
//        h. Do zadania pierwszego dodaj klasę University. Dodaj do niej metody:
//        void addStudent(long indexNumber, String name, String surname)
//        (możesz generować indexNumber)
//        boolean containsStudent(long indexNumber)
//        Student getStudent(long indexNumber)
//        int studentsCount()
//        void printAllStudent()
//        i*. dodaj wyjątek NoSuchStudentException (RuntimeException), dodaj go do metody getStudent()
//        j*. dodaj interfejs StudentFormat { String format(Student student) } i wykorzystaj go jako parametr
//        w metodzie void printAllStudent()
//        k. Wykonaj jeszcze raz zadanie pierwsze wykorzystując klasę University

public class Student {
private long numerIndeksu;
private String imie;
private String nazwisko;

    public Student() {

    }

    public Student(long numerIndeksu, String imie, String nazwisko) {
        this.numerIndeksu = numerIndeksu;
        this.imie = imie;
        this.nazwisko = nazwisko;
    }

    public long getNumerIndeksu() {
        return numerIndeksu;
    }

    public void setNumerIndeksu(long numerIndeksu) {
        this.numerIndeksu = numerIndeksu;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    @Override
    public String toString() {
        return "Student{" +
                "numerIndeksu=" + numerIndeksu +
                ", imie='" + imie + '\'' +
                ", nazwisko='" + nazwisko + '\'' +
                '}';
    }
}
