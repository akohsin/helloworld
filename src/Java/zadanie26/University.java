package Java.zadanie26;

//Zadanie 26:
//        a. Utwór klasę Student o polach: (long) numerIndeksu, imie, nazwisko oraz:
//        b. Utwórz kilku studentów i dodaj do mapy (HashMap).
//        c. sprawdź, czy mapa zawiera studenta o indeksie 100200,
//        d. wypisz studenta o indeksie 100400
//        e. wypisz liczbę studentów
//        f. wypisz wszystkich studentów
//        g. zmień implementacje na TreeMap
//        h. Do zadania pierwszego dodaj klasę University. Dodaj do niej metody:
//        void addStudent(long indexNumber, String name, String surname)
//        (możesz generować indexNumber)
//        boolean containsStudent(long indexNumber)
//        Student getStudent(long indexNumber)
//        int studentsCount()
//        void printAllStudent()
//        i*. dodaj wyjątek NoSuchStudentException (RuntimeException), dodaj go do metody getStudent()
//        j*. dodaj interfejs StudentFormat { String format(Student student) } i wykorzystaj go jako parametr
//        w metodzie void printAllStudent()
//        k. Wykonaj jeszcze raz zadanie pierwsze wykorzystując klasę University

import java.util.HashMap;
import java.util.Map;

public class University {
    Map<Long, Student> listaStudentow = new HashMap<>();

    void addStudent(long indexNumber, String name, String surname) {
        Student osoba = new Student(indexNumber, name, surname);
        listaStudentow.putIfAbsent(osoba.getNumerIndeksu(), osoba);
    }

    void addStudent(Student osoba) {
        listaStudentow.putIfAbsent(osoba.getNumerIndeksu(), osoba);
    }

    boolean containsStudent(long indexNumber) {
        if (listaStudentow.containsKey(indexNumber)) return true;
        else return false;
    }


    Student getStudent(long indexNumber) {
        Student student = listaStudentow.get(indexNumber);
        if(student==null) throw new NoSuchStudentException() ;

        return student;
    }

    int studentsCount() {
        return listaStudentow.size();
    }

    void printAllStudents() {
        for (Student student : listaStudentow.values()) System.out.println(student.toString());
    }
}
