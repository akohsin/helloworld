package Java.zadanie26;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

//Zadanie 26:
//        a. Utwór klasę Student o polach: (long) numerIndeksu, imie, nazwisko oraz:
//        b. Utwórz kilku studentów i dodaj do mapy (HashMap).
//        c. sprawdź, czy mapa zawiera studenta o indeksie 100200,
//        d. wypisz studenta o indeksie 100400
//        e. wypisz liczbę studentów
//        f. wypisz wszystkich studentów
//        g. zmień implementacje na TreeMap
//        h. Do zadania pierwszego dodaj klasę University. Dodaj do niej metody:
//        void addStudent(long indexNumber, String name, String surname)
//        (możesz generować indexNumber)
//        boolean containsStudent(long indexNumber)
//        Student getStudent(long indexNumber)
//        int studentsCount()
//        void printAllStudent()
//        i*. dodaj wyjątek NoSuchStudentException (RuntimeException), dodaj go do metody getStudent()
//        j*. dodaj interfejs StudentFormat { String format(Student student) } i wykorzystaj go jako parametr
//        w metodzie void printAllStudent()
//        k. Wykonaj jeszcze raz zadanie pierwsze wykorzystując klasę University
public class Main {
    public static void main(String[] args) {
        University uniwerek = new University();


        Student osoba = new Student(111110, "A", "K");
        Student osoba1 = new Student(100200, "b", "K");
        Student osoba2 = new Student(100400, "c", "K");
        Student osoba3 = new Student(111113, "d", "K");
        Student osoba4 = new Student(111114, "e", "K");

        Map<Long, Student> mapa = new HashMap<>();
        mapa.put(osoba.getNumerIndeksu(), osoba);
        mapa.put(osoba.getNumerIndeksu(), osoba1);
        mapa.put(osoba.getNumerIndeksu(), osoba2);
        mapa.put(osoba.getNumerIndeksu(), osoba3);
        mapa.put(osoba.getNumerIndeksu(), osoba4);

        for (Student x : mapa.values()) {
            if (x.getNumerIndeksu() == 100200) System.out.println("mapa zawiera studenta o numerze indeksu 100200");

        }
        for (Student x : mapa.values()) {
            if (x.getNumerIndeksu() == 100400) System.out.println(x.toString());
        }
        System.out.println("wielkosc mapy to:" + mapa.size());

        for (Student x : mapa.values()) System.out.println(x.toString());



        for (Student x : mapa.values()) {
            if (x.getNumerIndeksu() == 100400) System.out.println(x.toString());
        }

        TreeMap<Long, Student> treeMap = new TreeMap<>(mapa);

        Student osoba0 =  uniwerek.getStudent(0000);
        System.out.println(osoba0.toString());


    }


}


