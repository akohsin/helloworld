package Java.zadanie32;

import javax.swing.text.DateFormatter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

//Zadanie 32:
//        Stwórz aplikację która na:
//        - wpisanie date wypisuje obecny LocalDate
//        - wpisanie time wypisuje obecny LocalTime
//        - wpisanie datetime wypisuje obecny LocalDateTime
//        (W wybranym przez Ciebie formacie)
//        Jeśli użytkownik wpisze 'quit' to zakoncz program.
public class Main {
    public static void main(String[] args) {
        boolean isRunning = true;
        Scanner sc= new Scanner(System.in);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm, dd/MM/yy");
        DateTimeFormatter formatterDaty= DateTimeFormatter.ofPattern("dd/MM/yy");
        DateTimeFormatter formatterCzasu= DateTimeFormatter.ofPattern("HH:mm");
        do{
            switch (sc.nextLine()) {
                case "time":
                    LocalTime czas = LocalTime.now();
                    System.out.println(czas.format(formatterCzasu));
                    break;
                case "date":
                    LocalDate data = LocalDate.now();
                    System.out.println(data.format(formatterDaty));
                    break;
                case "datetime":
                    LocalDateTime czasData = LocalDateTime.now();
                    System.out.println(czasData.format(formatter));
                    break;
                case "quit":
                    isRunning=false;
                    break;
            }
        }

        while (isRunning);


    }
}
