package Java.zadanie34;
//Zadanie 34:
//        Stwórz program a w nim enum'a Plec, a w nim dwie wartości: "Kobieta", "Mezczyzna"
//        Stwórz program który skanuje z wejścia linie, a następnie sprawdza czy wprowadzona linia zawiera wartości enuma:
//        (posługując się parsowaniem enuma rzucającego IllegalArgumentException)
//        podpowiedz: String linia = scanner.nextLine();
//        Plec wpisana = Plec.valueOf(linia.trim());
//        System.out.println("Wpisano:" + wpisana);

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        try {
            String linia = sc.nextLine();
            Plec plec = Plec.valueOf(linia.trim());
        } catch (IllegalArgumentException iae) {
            System.out.println("blad");
        }


    }
}
