package Java.zadanie7;

//Zadanie 7:
//        Stwórz program, a w nim:
//        a) klasę kwadrat która jako parametr konstruktora przyjmuje długość krawędzi.
//        w klasie stwórz metodę "obliczObwod" i "obliczPole" ktore ZWRACA wartości. Stwórz maina i przetestuj działanie.
//        b) klasę prostokąt która przyjmuje w konstruktorze dwa parametry (długość obu boków) i stwórz
//        w klasie stwórz metodę "obliczObwod" i "obliczPole" ktore ZWRACA wartości. Stwórz maina i przetestuj działanie.
//        d) klasę koło, która jako parametr przyjmuje promień. Dodaj w klasie koło metodę "obliczObwod"
//        i "obliczPole" ktore ZWRACA wartości. Stwórz maina i przetestuj działanie.
//public class Main {
public class Rectangle {
    private int length;
    private int width;

    Rectangle(int width, int length) {
        this.length = length;
        this.width = width;
    }
    public int rectangleArea(int width, int length){
        return width*length;
    }
    public int rectangleCircum(int width, int length){
        return 2*(width+length);
    }


}
