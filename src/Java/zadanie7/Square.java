package Java.zadanie7;

//Zadanie 7:
//        Stwórz program, a w nim:
//        a) klasę kwadrat która jako parametr konstruktora przyjmuje długość krawędzi.
//        w klasie stwórz metodę "obliczObwod" i "obliczPole" ktore ZWRACA wartości. Stwórz maina i przetestuj działanie.
//        b) klasę prostokąt która przyjmuje w konstruktorze dwa parametry (długość obu boków) i stwórz
//        c) klasę kwadrat która jako parametr przyjmuje długość krawędzi.
//        w klasie stwórz metodę "obliczObwod" i "obliczPole" ktore ZWRACA wartości. Stwórz maina i przetestuj działanie.
//        d) klasę koło, która jako parametr przyjmuje promień. Dodaj w klasie koło metodę "obliczObwod"
//        i "obliczPole" ktore ZWRACA wartości. Stwórz maina i przetestuj działanie.
//public class Main {

public class Square {
    private int width;

    Square(int width) {
        this.width = width;
    }

    public int squareArea() {
        return width * width;
    }
    public int squareCircum(){
        return width*4;
    }
}
