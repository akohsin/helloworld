package Java.zadanie7;

public class Circle {
    private double radius;

    Circle(double radius) {
        this.radius = radius;
    }
    public double circleArea(double radius){
        return 3.14*radius*radius;
    }
    public double circleCircum(double radius){
        return 2*3.14*radius;
    }

}
