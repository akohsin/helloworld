package Java.zadanie15;

//Zadanie 15:
//        Stwórz klasę Telefon która posiada:
//        a)
//        - pole 'czy dzwoni'
//        - pole 'numer telefonu'
//        - metody getterów i setterów dla wszystkich pól
//        - metodę 'zadzwon' ktora zmienia wartość pola 'czy dzwoni' z false na true,
//        ale tylko wtedy kiedy jest false(nie mozna wykonac drugiego polaczenia jesli juz dzwoni)
//        - metode 'rozlacz' ktora zmienia wartosc pola 'czy dzwoni' na false z true,
//        ale tylko wtedy kiedy jest true (kiedy trwa jakas rozmowa)
//        Przetestuj aplikację w Main'ie
//        b*)
//        - pole 'zawartosc wyswietlacza' (String)
//        - pole 'wybrany numer' (String)
//        - zmodyfikuj metode zadzwon tak, zeby przyjmowala parametr (numer na ktory dzwonimy)
//        po zmienieniu flagi 'czy dzwoni' na true ustaw 'wybrany numer' na numer z parametru.
//        Ustaw pole 'zawartosc wyswietlacza' na tekst "Rozmowa z numer_telefonu".
//        Przetestuj aplikację w Main'ie
public class Phone {
    private Boolean czyDzwoni = false;
    private long numerTelefonu;
    private String zawartoscWyswietlacza;
    private String wybranyNumer;

    public void zadzwon(String wybranyNumer) {
        if (czyDzwoni == false) {
            czyDzwoni = true;
            this.wybranyNumer = wybranyNumer;
            this.zawartoscWyswietlacza = "Rozmowa z " + wybranyNumer;
        }
    }

    public void rozlacz() {
        if (czyDzwoni == true) czyDzwoni = false;
    }

    public Boolean getCzyDzwoni() {
        return czyDzwoni;
    }

    public long getNumerTelefonu() {
        return numerTelefonu;
    }

    public void setCzyDzwoni(Boolean czyDzwoni) {
        this.czyDzwoni = czyDzwoni;
    }

    public void setNumerTelefonu(long numerTelefonu) {
        this.numerTelefonu = numerTelefonu;
    }

    public String getWybranyNumer() {
        return wybranyNumer;
    }

    public String getZawartoscWyswietlacza() {
        return zawartoscWyswietlacza;
    }

    public void setWybranyNumer(String wybranyNumer) {
        this.wybranyNumer = wybranyNumer;
    }

    public void setZawartoscWyswietlacza(String zawartoscWyswietlacza) {
        this.zawartoscWyswietlacza = zawartoscWyswietlacza;
    }
}
