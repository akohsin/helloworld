package Java.zadanie11;

import java.util.Scanner;

//Zadanie 11*:
//        Stwórz program, w programie stwórz klasę "Field" które reprezentuje planszę (pole) podobne do takiego
//        jak gra w statki. Pole powinno posiadać tablicę dwuwymiarową KWADRATOWĄ zmiennych typu boolean.
//        Każde pole posiada wartość true/false jeśli było/nie było sprawdzane.
//        Wielkość tego pola powinna być określana w konstruktorze.
//
//        dodaj metodę:
//        - printField - która wypisuje na konsolę kwadrat i wartości pola.
//        X - jeśli komórka była sprawdzana
//        O - jeśli komórka nie była sprawdzana
//        - checkCell - która jako parametrY przyjmuje współrzędną
//        X oraz współrzędną Y komórki do sprawdzenia.
//        Jeśli komórka nie była jeszcze sprawdzana to ją sprawdza,
//        jeśli komórka była sprawdzana, to wypisuje komunikat że "komórka była sprawdzana".
//        - w klasie Main w metodzie main dodaj instancję klasy "Field" a następnie dodaj obsługę
//        komend ze Scanner'a, która pozwoli wywoływać metody printField oraz checkCell.
public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int width = 10;
        Field field = new Field(width);
        String inputLine = "";
        do {
            try {

                inputLine = scan.nextLine();
                String[] words = inputLine.split(" ");

                swwitch(field, words);


            } catch
                    (NumberFormatException nfe) {


            } catch (ArrayIndexOutOfBoundsException aioobe) {

            }
        } while (!inputLine.toLowerCase().startsWith("quit"));
    }

    public static void swwitch(Field field, String[] words) {
        switch (words[0]) {
            case "print":
                field.printField();
                break;
            case "check":
                int x = Integer.parseInt(words[1]);
                int y = Integer.parseInt(words[2]);
                field.checkCell(x, y);
                break;

        }
    }
}
