package Java.zadanie11;

//Zadanie 11*:
//        Stwórz program, w programie stwórz klasę "Field" które reprezentuje planszę (pole) podobne do takiego
//        jak gra w statki. Pole powinno posiadać tablicę dwuwymiarową KWADRATOWĄ zmiennych typu boolean.
//        Każde pole posiada wartość true/false jeśli było/nie było sprawdzane.
//        Wielkość tego pola powinna być określana w konstruktorze.
//
//        dodaj metodę:
//        - printField - która wypisuje na konsolę kwadrat i wartości pola.
//        X - jeśli komórka była sprawdzana
//        O - jeśli komórka nie była sprawdzana
//        - checkCell - która jako parametrY przyjmuje współrzędną
//        X oraz współrzędną Y komórki do sprawdzenia.
//        Jeśli komórka nie była jeszcze sprawdzana to ją sprawdza,
//        jeśli komórka była sprawdzana, to wypisuje komunikat że "komórka była sprawdzana".
//        - w klasie Main w metodzie main dodaj instancję klasy "Field" a następnie dodaj obsługę
//        komend ze Scanner'a, która pozwoli wywoływać metody printField oraz checkCell.
public class Field {
    private int width;
    boolean[][] battleField = new boolean[width][width];

    public Field(int x) {
        this.width = x;
        battleField = new boolean[width][width];

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < width; j++) {
                battleField[i][j] = false;
            }
        }
    }


    public void checkCell(int x, int y) {
        if (battleField[x][y] == true) System.out.println("already checked");
        else battleField[x][y] = true;
    }

    public void printField() {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < width; j++) {
                if (battleField[i][j]==true)System.out.print(" X ");
                else System.out.print(" O ");
                if (j == width - 1) System.out.print("\n");

            }

        }
    }


}
