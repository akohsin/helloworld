package Java.zadanie30;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Scanner;

//Zadanie 30 (Time):
//        Stwórz aplikacje która ze scannera przyjmuje datę w formacie
//        '2013-03-20 12:40' (ROK-MSC-DZIEN GODZINA:DATA) i wypisuje komunikat ze data jest w poprawnym formacie lub (jeśli nie jest)
//        rzuca Exception "WrongDateTimeException" który jest wyjątkiem, który dziedziczy po RuntimeException (napisz go sam/a).
public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        boolean isRunning = true;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:MM");

        do {
            String wpis = scan.nextLine();
            if (wpis.equals("quit")) isRunning = false;
            else {

                try {
                    LocalDateTime czas = LocalDateTime.parse(wpis, formatter);
                    System.out.println("obecny czas to:" + czas);

                } catch (DateTimeParseException dtpe) {
                    System.out.println("zle formatowanie");
                    throw new WrongDateTimeException();
                }
            }
        } while (isRunning);

    }
}
