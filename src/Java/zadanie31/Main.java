package Java.zadanie31;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

//Zadanie 31:
//        Stwórz aplikację która na wciśnięcie klawisza "Enter" wypisuje obecną datę i godzinę w (wybranym przez Ciebie) formacie.
//        Jeśli użytkownik wpisze 'quit' to zakoncz program.
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        boolean isRunning = true;

        do {
            String enter = sc.nextLine();
            if (!enter.equals("quit")) {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
                if (enter.equals("Enter")) {

                    LocalDateTime czas = LocalDateTime.now();
                    System.out.println(czas.format(formatter));
                }

            }
            if (enter.equals("quit")) isRunning = false;
        } while (isRunning);

    }
}

