package Java.zadanie13;
//Zadanie 13:
//        Stwórz klasę Student która posiada:
//        - pole numer indeksu
//        - pole imie
//        - pole nazwisko
//        - tablicę ocen (stwórz tablicę maksymalnie 100 ocen)
//        - metody getterów i setterów
//        - metodę do obliczania średniej
//        - metodę która zwraca true jeśli żadna z ocen w tablicy ocen nie jest 1 ani 2,
//        oraz false w przeciwnym razie.

public class Student {
    private int numer;
    private String imie;
    private String nazwisko;
    private int[] tablica;
    private boolean checkStatus;

    public Student(int numer, String imie, String nazwisko, int[] tablica) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.tablica = tablica;
        this.numer = numer;
    }

    public int getNumer() {
        return numer;
    }

    public int[] getTablica() {
        return tablica;
    }

    public String getImie() {
        return imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public void setNumer(int numer) {
        this.numer = numer;
    }

    public void setTablica(int[] tablica) {
        this.tablica = tablica;
    }

    public boolean checkGrades() {
        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i] <= 2) checkStatus = false;
            else checkStatus = true;
        }
        return checkStatus;

    }
}





