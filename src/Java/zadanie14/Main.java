package Java.zadanie14;
//        Przetestuj aplikację w Main'ie.
//        - dodaj domyślny (pusty) konstruktor, oraz konstruktor z odbiorcą i flagą zawartość,
//        - ma metodę 'wyślij' która sprawdza czy jest zawartość, i czy jest odbiorca i wyświetla
//        komunikat o nadaniu jeśli jest zawartość i odbiorca,
//        oraz ustawia flagę 'czy została wysłana' na true.
//        - ma metodę 'wyślij polecony' która sprawdza to samo co wyżej, ale sprawdza również czy
//        nadawca jest ustawiony i pozwala wysłać tylko jeśli wszystkie pola są ustawione.
//        Przetestuj aplikację w Main'ie.

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Paczka paczucha = new Paczka();
        String inputLine;
        String inputLine2;
        String inputLine3;
        Boolean statusWysylki = false;
        Boolean flaga = true;


        do {
            try {
                System.out.println("podaj komende: nadawca, odbiorca, zawartosc, wyslij, wyslij polecony, czy została wyslana, quit");
                inputLine = scan.nextLine();
                switch (inputLine) {
                    case "odbiorca":
                        System.out.println("podaj komende: zapisz, odczytaj");
                        inputLine2 = scan.nextLine();
                        if (inputLine2.equals("zapisz")) {
                            System.out.println("podaj odbiorce");
                            inputLine3 = scan.nextLine();
                            paczucha.setOdbiorca(inputLine3);
                        } else if (inputLine2.equals("odczytaj")) {
                            System.out.println("odbiorca to: " + paczucha.getOdbiorca());
                        } else System.out.println("wpisz właściwą komende");
                        break;
                    case "nadawca":
                        System.out.println("podaj komende: zapisz, odczytaj");
                        inputLine2 = scan.nextLine();
                        if (inputLine2.equals("zapisz")) {
                            System.out.println("podaj odbiorce");
                            inputLine3 = scan.nextLine();
                            paczucha.setNadawca(inputLine3);
                        } else if (inputLine2.equals("odczytaj")) {
                            System.out.println("nadawca to: " + paczucha.getNadawca());
                        } else System.out.println("wpisz właściwą komende");
                        break;
                    case "zawartosc":
                        System.out.println("podaj komende: zapisz, odczytaj");
                        inputLine2 = scan.nextLine();
                        if (inputLine2.equals("odczytaj")) {
                            System.out.println("czy paczka posiada zawartosc: " + paczucha.getZawartosc());
                        } else {
                            System.out.println("czy umiesciles zawartosc ? tak/nie");
                            inputLine2 = scan.nextLine();
                            if (inputLine2.equals("tak")) paczucha.setZawartosc(true);
                            else if (inputLine2.equals("nie")) paczucha.setZawartosc(false);
                            else System.out.println("wpisz właściwą komende");
                        }
                        break;
                    case "wyslij":
                        if (paczucha.getZawartosc() != false && paczucha.getOdbiorca() != null) {
                            System.out.println("paczka została nadana");
                            statusWysylki = true;
                        } else System.out.println("brak odbiorcy lub zawartosci, paczka nie została wysłana");
                        break;
                    case "wyslij polecony":
                        if (paczucha.getZawartosc() != false && paczucha.getOdbiorca() != null && !paczucha.getNadawca().equals(null)) {
                            System.out.println("paczka została nadana");
                            statusWysylki = true;
                        } else System.out.println("brak odbiorcy, zawartosci lub nadawcy, paczka nie została wysłana");
                        break;
                    case "czy zostala wyslana":
                        if (paczucha.getStatusWyslana() == true) System.out.println("paczka zosyala wyslana");
                        else System.out.println("paczka nie została wysłan");
                        break;
                    case "quit":
                        flaga = false;

                }
            } catch (NumberFormatException nfe) {
                System.out.println("błąd formatu");
            } catch (ArrayIndexOutOfBoundsException aioobe) {
                System.out.println("błąd rozmiaru tablicy");
            }
//            inputLine = "";
//            inputLine2 = "";
//            inputLine3 = "";
        } while (flaga == true);
    }
}
