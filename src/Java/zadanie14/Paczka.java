package Java.zadanie14;
//Zadanie 14:
//        Stwórz klasę 'Paczka' która:
//        - ma pole odbiorca
//        - ma pole nadawca
//        - ma pole 'czy została wysłana'
//        - ma flagę 'czy jest zawartość'
//        - dodaj gettery i settery
//        Przetestuj aplikację w Main'ie.
//        - dodaj domyślny (pusty) konstruktor, oraz konstruktor z odbiorcą i flagą zawartość,
//        - ma metodę 'wyślij' która sprawdza czy jest zawartość, i czy jest odbiorca i wyświetla
//        komunikat o nadaniu jeśli jest zawartość i odbiorca,
//        oraz ustawia flagę 'czy została wysłana' na true.
//        - ma metodę 'wyślij polecony' która sprawdza to samo co wyżej, ale sprawdza również czy
//        nadawca jest ustawiony i pozwala wysłać tylko jeśli wszystkie pola są ustawione.
//        Przetestuj aplikację w Main'ie.

public class Paczka {
    private String odbiorca;
    private String nadawca;
    private Boolean statusWyslana = false;
    private Boolean zawartosc = false;

    public Paczka() {

    }

    public void wyslij() {
        if (zawartosc == true && !(odbiorca==null)) System.out.println("wyslana");
        statusWyslana = true;
    }

    public void wyslijPolecony() {
        if (zawartosc == true && !odbiorca.equals("") && !nadawca.equals("null")) System.out.println("wyslana");
        statusWyslana = true;
    }

    public Paczka(String odbiorca, boolean zawartosc) {
        this.odbiorca = odbiorca;
        this.zawartosc = zawartosc;
    }

    public Boolean getStatusWyslana() {
        return statusWyslana;
    }

    public Boolean getZawartosc() {
        return zawartosc;
    }

    public String getNadawca() {
        return nadawca;
    }

    public String getOdbiorca() {
        return odbiorca;
    }

    public void setNadawca(String nadawca) {
        this.nadawca = nadawca;
    }

    public void setOdbiorca(String odbiorca) {
        this.odbiorca = odbiorca;
    }

    public void setStatusWyslana(Boolean statusWyslana) {
        this.statusWyslana = statusWyslana;
    }

    public void setZawartosc(Boolean zawartosc) {
        this.zawartosc = zawartosc;
    }
}
