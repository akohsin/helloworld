package Java.zadanie22;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Talia {
    Random rand = new Random();
    List<Karta> taliaKart = new ArrayList<>();

    private int liczbaTalii;

    public int getLiczbaTalii() {
        return liczbaTalii;
    }

    private void setLiczbaTalii(int liczbaTalii) {
        this.liczbaTalii = liczbaTalii;
    }

    private int obliczTalie(){
        return liczbaTalii*52;
    }

    public Talia(int liczbaTalii){
        this.liczbaTalii=liczbaTalii;
    }


    //generowanie wyjściowej tali
    public void generujTalie() {
        String nazwa = "";
        for (int i = 2; i <= 14; i++) {
            switch (i) {
                case 2:
                    nazwa = "Dwojka";
                    break;
                case 3:
                    nazwa = "Trojka";
                    break;
                case 4:
                    nazwa = "Czworka";
                    break;
                case 5:
                    nazwa = "Piatka";
                    break;
                case 6:
                    nazwa = "Szostka";
                    break;
                case 7:
                    nazwa = "Siodemka";
                    break;
                case 8:
                    nazwa = "Osemka";
                    break;
                case 9:
                    nazwa = "Dziewiatka";
                    break;
                case 10:
                    nazwa = "Dziesiatka";
                    break;
                case 11:
                    nazwa = "Jopek";
                    break;
                case 12:
                    nazwa = "Dama";
                    break;
                case 13:
                    nazwa = "Krol";
                    break;
                case 14:
                    nazwa = "As";
                    break;
            }
            for (int j = 1; j <= 4*liczbaTalii; j++) {
                Karta karta = new Karta();
                karta.nazwa = nazwa;
                karta.wartosc = i;
                taliaKart.add(karta);
            }
        }
    }

    public List<Karta> losujTalie(int liczbaGraczy) {
        List<Karta> taliaGracza = new ArrayList<>();
        for (int i = 0; i <  obliczTalie() / liczbaGraczy; i++) {
            int zmienna = rand.nextInt(taliaKart.size());
            taliaGracza.add(taliaKart.get(zmienna));
            taliaKart.remove(zmienna);
        }
        return taliaGracza;
    }


}
