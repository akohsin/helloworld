package Java.wstep;

import java.util.Scanner;

public class zadanie36 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("podaj wspolczynniki a b c");
        while (scan.hasNextLine()) {
            try {
                String wyrazenie = scan.nextLine();
                String[] kolumny = wyrazenie.split(" ");
                double a = Double.parseDouble(kolumny[0]);
                double b = Double.parseDouble(kolumny[1]);
                double c = Double.parseDouble(kolumny[2]);
                double delta = b * b - 4 * a * c;
                if (delta == 0) {
                    double x0 = -b / 2 * a;
                    System.out.println("delta = " + delta + " x = " + x0);
                } else if (delta > 0) {
                    double pierw = Math.sqrt(delta);
                    double x1 = (-b - pierw) / (2 * a);
                    double x2 = (-b + pierw) / (2 * a);
                    System.out.println("delta = " + delta + " x1 = " + x1 + " x2 = " + x2);
                } else System.out.println("brak rozwiazan");
            } catch (NumberFormatException nfe) {
                System.out.println("podaj 3 wspolczynniki rownania kwadratowego");
            }
        }
    }
}
//Zadanie 36:
//        Stwórz program obliczający równanie kwadratowe (http://matematyka.pisz.pl/strona/54.html).
//        Na wejściu użytkownik podaje wartość zmiennych A, B i C,
//        a na wyjściu wypisują sie odpowiednie komunikaty o wartościach x1 i x2 lub x0, a także wartość delty.