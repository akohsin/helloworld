package Java.wstep;

import java.util.Random;

public class zadanie24 {
    public static void main(String[] args) {
        Random rand = new Random();
        int tablica[] = new int[10];
        int suma = 0;
        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < 10; i++) {
            tablica[i] = rand.nextInt(21) - 10;
            suma += tablica[i];
            System.out.println("element nr" + (i + 1) + " " + tablica[i]);
            if (tablica[i] < min) min = tablica[i];
            if (tablica[i] > max) max = tablica[i];
        }
        double srednia = (double) suma / 10;
        int mniejszych = 0;
        int wiekszych = 0;

        for (int i = 9; i >= 0; i--) {
            if (tablica[i] < srednia) mniejszych++;
            else if (tablica[i] > srednia) wiekszych++;
            System.out.println("element nr " + (i + 1) + " " + tablica[i]);
        }
        System.out.println("mniejszych od sredniej " + mniejszych);
        System.out.println("wiekszych od sredniej " + wiekszych);
        System.out.println("srednia" + srednia);

        char znaczek = 'a';
        int asci = (int) znaczek;
        System.out.println(asci);
    }
}
