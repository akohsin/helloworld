package Java.wstep;

import java.util.Scanner;

public class zadanie34 {
    public static void main(String[] args) {
//        Zadanie 34:
//        Napisz program, który umożliwia szyfrowanie podanego ciągu znaków przy użyciu
//        szyfru Cezara, który jest szczególnym przypadkiem szyfru podstawieniowego
//        monoalfabetycznego.
//                Użytkownik program podaje tekst do zaszyfrowania oraz liczbę n, o którą przesunięty
//        jest alfabet za pomocą którego szyfrujemy tekst. Dla uproszczenia można
//        przyjąć, że łańuch wejściowy składa się tylko z małych liter alfabetu angielskiego,
//        tj. ’a’ – ’z’ (26 znaków) oraz spacji.
//        Przykład 1.
//        Podaj łańcuch znaków do zaszyfrowania: abrakadabraz
//        Podaj przesunięcie: 2
//        Zaszyfrowany tekst: cdtcmcfcdtcb
//        Przykład 2.
//        Podaj łańcuch znaków do zaszyfrowania: cdtcmcfcdtcb
//        Podaj przesunięcie: -2
//        Zaszyfrowany tekst: abrakadabraz

        Scanner scan = new Scanner(System.in);
        System.out.println("podaj łańcuch");
        String slowo = scan.nextLine();
        char tablicaslowa[] = new char[slowo.length()];
        System.out.println("podaj przesuniecie");
        int przesuniecie = scan.nextInt();

        for (int i = 0; i < slowo.length(); i++) {
            if (((int) slowo.charAt(i) + przesuniecie) > 122)
                tablicaslowa[i] = (char) ((int) slowo.charAt(i) + przesuniecie - 97);
            else if (((int) slowo.charAt(i) + przesuniecie) < 97)
                tablicaslowa[i] = (char) ((int) slowo.charAt(i) + przesuniecie + 26);
            else tablicaslowa[i] = (char) ((int) slowo.charAt(i) + przesuniecie);
        }
        for (int i = 0; i < slowo.length(); i++) System.out.print(tablicaslowa[i]);

    }
}