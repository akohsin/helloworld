package Java.wstep;

import java.util.Scanner;

public class zadanie32 {
    public static void main(String[] args) {
//        Zadanie 32:
//        Napisać program, który sumuje cyfry w tekście podanym przez użytkownika.
//                Przykład:
//        "Ala ma 1 psa i 2 koty. Jola ma 10 rybek i 2 papugi."
//        Wynik:
//        6
        int licznik=0;
        Scanner scan= new Scanner(System.in);
        String slowo = scan.nextLine();

        char tablicaslowa[] = new char[slowo.length()];
        for(int i=0; i<slowo.length();i++) {
            tablicaslowa[i] = slowo.charAt(i);
            System.out.print(tablicaslowa[i]);
            if((int) tablicaslowa[i]>=48&& (int) tablicaslowa[i]<=57) licznik+=tablicaslowa[i]-48;
        }
        System.out.println("suma cyfr:");
        System.out.println(licznik);

    }
}
//48 57
