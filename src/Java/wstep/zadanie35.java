package Java.wstep;

import java.util.Scanner;

public class zadanie35 {
    public static void main(String[] args) {
//        Zadanie 35:
//        1.  Napisz program, który prosi użytkownika o dwie liczby a i b, i wyświetla wynik dzielenia a/b.
//            Jeżeli b jest ujemne program powinien wyświetlić odpowiedni komunikat.

//                a.  Wariant 1: Zastosuj instrukcję if
//        b.  Wariant 2: Zastosuj instrukcję try-catch}

        Scanner scan = new Scanner(System.in);
        System.out.println("podaj dwie liczby a i b");


        while (scan.hasNextLine()) {
            String liczby = scan.nextLine();
            String kolumny[] = liczby.split(" ");
            int a;
            int b;
            double wynik;

            if (kolumny[0].equals("quit")) {
                System.out.println("Exit");
                break;
            }
            if (kolumny[0].equals("skip")) {
                System.out.println("Skip");
                continue;
            }

            try {
                a = Integer.parseInt(kolumny[0]);
                System.out.println("wartość a=" + a);
                b = Integer.parseInt(kolumny[1]);
                if (b <= 0) {
                    throw new NumberFormatException();
                }
                System.out.println("wartosc b=" + b);
                wynik = (double) a / b;
                System.out.println("wynik dzielenia a/b wynosi: " + wynik);
            } catch (NumberFormatException nfe) {
                System.out.println("podaj liczbe calkowitą a oraz dodatnia b");
            }
        }


    }
}