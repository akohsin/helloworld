package Java.wstep;

import java.util.Scanner;

public class zadanie33 {

//    Zadanie 33:
//    Napisz program, który sprawdzi, czy w podanym przez użytkownika wyrażeniu
//    arytmetycznym nawiasy są poprawnie sparowane. Wyrażenie podawane jest jako
//    pojedynczy łańcuch znaków. Program powinien wyświetlić stosowny komunikat.
//    Przykład a:
//            "2 * (3.4 - (-7)/2)*(a-2)/(b-1)))"
//    Wynik:
//    Błędne sparowanie nawiasów
//    Przykład b:
//            "2 * (3.4 - (-7)/2)*(a-2)/(b-1))"
//    Wynik:
//    OK

public static void main(String[] args) {
    int licznik1=0;
    int licznik2=0;
    Scanner scan= new Scanner(System.in);
    String slowo = scan.nextLine();

    char tablicaslowa[] = new char[slowo.length()];
    for(int i=0; i<slowo.length();i++) {
        tablicaslowa[i] = slowo.charAt(i);
        if((int) tablicaslowa[i]==40) licznik1++;
        if((int) tablicaslowa[i]==41) licznik2++;
    }
    if(licznik1==licznik2) System.out.println("ok");
    else System.out.println("nok");
    }

}


//40 41