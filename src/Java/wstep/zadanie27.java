package Java.wstep;

import java.util.Scanner;

public class zadanie27 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("podaj wielkość tabliczki mnożenia");
        int x=scan.nextInt();
        for(int i=1;i<=x;i++){
            for(int j=1;j<=x;j++){
                if(i*j>99)System.out.print(" "+i*j+"  ");
                else if(i*j>9)System.out.print("  "+i*j+"  ");
                else System.out.print("   "+i*j+"  ");
                if(j==x)System.out.print("\n");
            }
        }
    }
}
