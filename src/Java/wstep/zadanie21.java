package Java.wstep;

import java.util.Scanner;

public class zadanie21 {
    public static void main(String[] args) {
        Scanner scan=new Scanner(System.in);
        System.out.println("podaj dlugosc podstawy trojkata");
        int N = scan.nextInt();
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < 2 * N - 1; j++) {
                if ((Math.abs((2 * N - 1) / 2 - j)) > i) System.out.print(" ");
                else System.out.print("*");
                if (j == 2 * N - 2) System.out.print("\n");
            }
        }
    }
}