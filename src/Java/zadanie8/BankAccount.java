package Java.zadanie8;
//Zadanie 8:
//        Stwórz program a w nim klasę "BankAccount".
//        konto bankowe powinno mieć metodę dodawania pieniędzy do konta, oraz odejmowania :
//        (metoda addMoney która w parametrze przyjmuje ilosc pieniedzy do dodania oraz
//        substractMoney która jako parametrz przyjmuje ilosc pieniedzy do odjecia).
//     Dodaj do klasy metodę "printBankAccountStatus" która powinna wypisywać stan konta.

public class BankAccount {
    private double balance = 0.0;
    public void addMoney(double input){
        balance+=input;
    }
    public void subMoney(double output){
        balance-=output;
    }
    public void printBankAccountStatus(){
        System.out.println("Bank account balanse equals: "+ balance);
    }


}
