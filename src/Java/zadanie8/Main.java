package Java.zadanie8;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        BankAccount account = new BankAccount();
        Scanner scan = new Scanner(System.in);
        String inputLine;
        String[] words;
        String operation;
        String word = "0";
        do {
            System.out.println("define operation: addition , substraction , status: ");
            operation = scan.nextLine();
            if (!operation.equals("status")) {
                System.out.println("define amount of money you want to operate");
                word = scan.nextLine();
            }

            try {
                double value = Double.parseDouble(word);
                switch (operation) {
                    case "addition": {
                        account.addMoney(value);
                        break;
                    }
                    case "substraction": {
                        account.subMoney(value);
                        break;
                    }
                    case "status": {
                        account.printBankAccountStatus();
                        break;
                    }
                }
            } catch (NumberFormatException nfe) {
                System.out.println("wrong value");
            }

        }
        while (!operation.equals("quit"));
    }
}
