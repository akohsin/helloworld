package Java.zadanie18;

import java.util.ArrayList;
import java.util.Random;

//Zadanie 18 (LISTY - omówimy 06.10.2017) (na przykładzie ArrayList):
//        Napisz metody (użyj pętli for i forEach):
//        - Liczącą sumę liczb na liście
//        - Liczącą iloczyn liczb na liście
//        - Liczącą średnią liczb na liście
//        * Napisz metodę liczącą:
//        - Negujący wszystkie wartości logiczne na liście
//        - Koniunkcję wartości logicznych na liście
//        - Alternatywę wartości logicznych na liście
//        Napisz klasę User o polach String name, String password.
//        Utwórz listę typu User, dodaj do niej kilka obiektów, a następnie wypisz.
//
//        Napisz program tworzący dziesięcioelementową listę i tablice liczb,
//        a następnie sprawdzający ilu pozycjach wartości są identyczne.
//        Przykład:
//        Lista {1,2,4,2,5,12,3,2}
//        Tablica {4,2,2,1,5,29,3,8}
//        Wynik: 3

//        *Napisz metodę która:
//        - kopiuje listę
//        - przepisuje listę do tablicy (podanej jako parametr)
//        - przepisuje tablicę do listy (nowo utworzonej)
//        - przepisuje dwie listy do jednej
//        - przepisuje dowolną ilość list do jednej (varargs)
//        - sprawdza, czy dwie listy są takie same
public class Main {
    public static void main(String[] args) {
        ArrayList<User> listOfUsers = new ArrayList();
                User person = new User();
        person.setName("Gumiak");
        User person2 = new User();

        person2.setName("Cytryn");
        listOfUsers.add(person);
        listOfUsers.add(person2);
        for (User user : listOfUsers) {
            System.out.println(user.getName());
        }
        Random rand = new Random();
        ArrayList<Integer> lista = new ArrayList();
        int[] tablica = new int[10];
        int licznik = 0;
        for (int i = 0; i < 10; i++) {
            int value1 = rand.nextInt(10);
            Integer Value2 = rand.nextInt(10);
            tablica[i] = value1;
            lista.add(Value2);
            if (tablica[i] == lista.get(i)) licznik++;

        }
        System.out.println(licznik);

//        ArrayList<Boolean> lista4 = new ArrayList<>();
//        lista4.add(true);
//        lista4.add(false);
//        lista4.add(true);
//        lista4.add(false);
//        lista4.add(false);
//        lista4.add(false);
//        System.out.println(lista4);
//        negate(lista4);
//        System.out.println(lista4);

    }


    public static double multiplyArrayContainers(ArrayList<Integer> listOfInts) {
        double iloczyn = 1;
        for (int i = 0; i < listOfInts.size(); i++) {
            iloczyn *= listOfInts.get(1);
        }
        return iloczyn;
    }

    public static int sumNumbersInIntsList(ArrayList<Integer> listOfInts) {
        int suma = 0;
        for (int i = 0; i < listOfInts.size(); i++) {
            suma++;
        }
        return suma;
    }

    public static double averageOfArray(ArrayList<Integer> listOfInts) {

        double srednia = 0;
        double suma = 0;
        for (int i = 0; i < listOfInts.size(); i++) {
            suma += listOfInts.get(i);
        }
        return srednia = suma / listOfInts.size();
    }

//    public static void negate(ArrayList<Boolean> listOfBoolean) {
//        int licznik5 = 0;
//        for (boolean wartosc : listOfBoolean) {
//            listOfBoolean<> = !wartosc;
//            licznik++;
//        }
//    }

    public static void alternate(ArrayList<Boolean> listOfBoolean) {
        boolean wynik = false;

        for (boolean wartosc : listOfBoolean) {
            wynik = wynik || wartosc;

        }
    }

    public static void conjunction(ArrayList<Boolean> listOfBoolean) {
        boolean wynik = true;
        for (boolean wartosc : listOfBoolean) {
            wynik = wynik && wartosc;
        }
    }

    public static void metoda1(ArrayList lista1, ArrayList lista2) {
        for (Object x : lista1) {
            lista2.add(x);

        }
    }

    public static void metoda2(ArrayList<Integer> lista, int[] tablica) {
        for (int x : tablica) {
            lista.add(x);
        }
    }

    public static ArrayList metoda3(ArrayList lista1, ArrayList lista2) {
        ArrayList lista3 = new ArrayList();
        for (Object x : lista1) lista3.add(x);
        for (Object x : lista2) lista3.add(x);
        return lista3;
    }

    public static ArrayList metoda4(ArrayList... lista1) {
        ArrayList lista = new ArrayList();
        for (ArrayList x : lista1) {
            for (Object y : x) {
                lista.add(x);
            }
        }
        return lista;
    }
    public static boolean metoda5(ArrayList lista1, ArrayList lista2){
        boolean flaga=true;
        if (lista1.size()==lista2.size()){
            for (int i=0;i<lista1.size();i++) {
                if (lista1.get(1)!=lista2.get(i));flaga=false;

            }

        }else flaga=false;
        return flaga;
    }


}


