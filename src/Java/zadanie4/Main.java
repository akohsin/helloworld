package Java.zadanie4;


import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        //a)
//        int addResult = calculator.addTwoNumbers(20, 30);
//        System.out.println("Wynik dodawania: " + addResult);
//
//
//        int substractResult = calculator.substractTwoNumbers(20, 30);
//        System.out.println("Wynik odejmowania: " + substractResult);
//
//
//        int multiplyResult = calculator.multiplyTwoNumbers(20, 30);
//        System.out.println("Wynik mnożenia: " + multiplyResult);
//
//
//        int divideTwoNumbers = calculator.divideTwoNumbers(20, 30);
//        System.out.println("Wynik dzielenia: " + divideTwoNumbers);
//
//        int divideByZero = calculator.divideTwoNumbers(20, 0);
//        System.out.println("Wynik dzielenia: " + divideByZero);
        //b)
        Scanner scanner = new Scanner(System.in);

        String inputLine;
        do {
            System.out.println("This is simple calculator, use [add/sub/mul/div] " +
                    "followed by two numbers:");
            inputLine = scanner.nextLine(); // przyjmowanie komendy z wejscia

            String[] words = inputLine.split(" ");
            // command
            String command = words[0];

            if (command.equals("quit")) {
                // wyskocz z pętli while
                break;
            }

            // numbers
            String firstNumber = words[1];
            String secondNumber = words[2];

            //
            int first;
            int second;

            try {
                first = Integer.parseInt(firstNumber);
                second = Integer.parseInt(secondNumber);
            } catch (NumberFormatException nfe) {
                System.out.println("Wrong number format.");
                // obsługa niepoprawnego wejścia
                continue;
            }

            if (command.equals("add")) {
                // dodawanie

                System.out.println(calculator.addTwoNumbers(first, second));
            } else if (command.equals("sub")) {
                // odejmowanie

                System.out.println("Result: " + calculator.substractTwoNumbers(first, second));
            } else if (command.equals("mul")) {
                // mnozenie

                System.out.println("Result: " + calculator.multiplyTwoNumbers(first, second));
            } else if (command.equals("div")) {
                // dodawanie

                System.out.println("Result: " + calculator.divideTwoNumbers(first, second));
            } else {
                System.out.println("Unknown command.");
            }
        } while (!inputLine.equals("quit")); // wpisanie quit spowoduje wyjscie z kalkulatora/programu

    }
}