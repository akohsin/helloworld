package Java.zadanie25;
//Zadanie 25:
//        a. Umieść wszystkie elementy tablicy {10,12,10,3,4,12,12,300,12,40,55} w zbiorze (HashSet) oraz:
//        b. Wypisz liczbę elementów na ekran (metoda size())
//        c. Wypisz wszystkie zbioru elementy na ekran (forEach)
//        d. Usuń elementy 10 i 12, wykonaj ponownie podpunkty a) i b)
//
//        e. Napisz metodę sprawdzającą, czy w tekście nie powtarzają się litery z wykorzystaniem zbioru.
// (boolean containDuplicates(String text))
//        f. Utwórz klasę ParaLiczb (int a,int b) i dodaj kilka instancji do zbioru:
//        {(1,2), (2,1), (1,1), (1,2)}.
//        g. Wyświetl wszystkie elementy zbioru na ekran. Czy program działa zgodnie z oczekiwaniem


import java.util.*;

public class Main {
    public static void main(String[] args) {
        HashSet<Integer> zbior = new HashSet<Integer>();
        Set<ParaLiczb> zbiorPar = new LinkedHashSet<>();


        zbiorPar.add(new ParaLiczb(1, 2));
        zbiorPar.add(new ParaLiczb(1, 1));
        zbiorPar.add(new ParaLiczb(2, 1));
        zbiorPar.add(new ParaLiczb(1, 2));
        for (ParaLiczb x : zbiorPar) {
            System.out.println(x.getA() + " " + x.getB());

        }

        int[] tablica = {10, 12, 10, 3, 4, 12, 12, 300, 12, 40, 55};

        for (int x : tablica) zbior.add(x);

        System.out.println("wielkosc zbioru to:" + zbior.size());

        for (Integer i : zbior) System.out.print(i + " ");
        System.out.println();
        zbior.remove(10);
        zbior.remove(12);

        for (int x : tablica) zbior.add(x);

        System.out.println("wielkosc zbioru to:" + zbior.size());

        for (Integer i : zbior) System.out.print(i + " ");
        System.out.println();

        Set<Object> nowyZbior = zbior(1.42314, 1.21312, 1.4234);

    }


    // sprawdzanie duplikatów znaku w słowie/zdaniu
    public static boolean containsDuplicates(String text) {
        // pozbywam się kropek, przecinków oraz spacji ze słowa
        // a następnie trim - usuwam białe znaki (spacje i taby)
        // przed i po zdaniu/słowie
        String trimmed = text.replace(",", "")
                .replace(".", "")
                .replace(" ", "").trim();

        // zamieniam ciąg znaków (String) na tablicę znaków
        char[] charArray = trimmed.toCharArray();

        // tworzę listę do której będę umieszczał wszystkie litery
        // (wraz z duplikatami)
        List<Character> listOfChars = new ArrayList<>();
        for (char c : charArray) {
            // dodaje każdy znak z tablicy do listy
            listOfChars.add(c);
        }
        // umieszczam wszytkie znaki z listy do zbioru
        // w zbiorze nie moze byc duplikatow wiec wszystkie znaki
        // zduplikowane zostana usuniete
        Set<Character> zbior = new HashSet<>(listOfChars);

        // z tego wzgledu (usuniecia duplikatow) rozmiar listy i zbioru
        // bedzie sie od siebie roznic. Jesli sa rowne, to znaczy ze nie
        // ma duplikatu
        return zbior.size() != listOfChars.size();
    }

    public static void printDuplicates(String text) {
        String trimmed = text.replace(",", "")
                .replace(".", "")
                .replace(" ", "").trim();
        char[] charArray = trimmed.toCharArray();

        List<Character> listOfChars = new ArrayList<>();
        Map<Character, Integer> map = new HashMap<>();
        for (char c : charArray) {
            listOfChars.add(c);

            if (map.containsKey(c)) {
                Integer ileWystapienJuzJest = map.get(c);
                map.put(c, ileWystapienJuzJest + 1);
            } else {
                map.put(c, 1);
            }
        }

        for (Map.Entry<Character, Integer> entry : map.entrySet()) {
            if (entry.getValue() >= 2) {
                System.out.println(entry.getKey());
            }
        }
//        return zbior.size() == listOfChars.size();
    }
    //        h*. Napisz metodę, która przyjmuje dowolną liczbę liczb typu double (varargs) i tworzy zbiór
// (HashSet), składający się z tych elementów, lub wyrzuca IllegalArgumentException, jeżeli elementy nie są unikalne
//public static Set<Double> newSet(double numbers…)
//        - spróbuj przerobić metodę na wersję generyczną
//public static <T> Set<T> newSet(T... data

    public static <T> Set<T> zbior(T... obiekty) {

        Set<T> zbiorObiektow = new HashSet<>();

        for (T x : obiekty) {

            int rozmiarPrzed = zbiorObiektow.size();

            zbiorObiektow.add(x);

            if (zbiorObiektow.size() == rozmiarPrzed) throw new IllegalArgumentException();
        }
        return zbiorObiektow;


    }


}