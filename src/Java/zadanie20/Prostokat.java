package Java.zadanie20;

public class Prostokat extends Figure{

    private int bok;
    private int bok2;

    public Prostokat(int bok, int bok2) {
        this.bok = bok;
        this.bok2=bok2;
    }


    @Override
    public double obliczObwod() {
        obwod=2*(bok+bok2);
        return obwod;
    }

    @Override
    public double obliczPole() {
        pole=bok*bok2;
        return pole;
    }
}
