package Java.zadanie20;
//Zadanie 20:
//        Stwórz klasę nadrzędną Figure która posiada dwie metody abstrakcyjne:
//        - metoda obliczPole
//        - metoda obliczObwód
//        Stwórz klasy podrzędne "Kwadrat", "Prostokąt", "Koło" które dziedziczą po Figure i
//        zaimplementuj w nich metody obliczObwód i obliczPole. Stwórz maina, w tym mainie stwórz Listę
//        obiektów Figure i dodaj kilka figur, a następnie w pętli foreach wypisz pola i obwody wszystkich figur.
//        ** Zastosuj instanceof aby sprawdzić typ figury i wypisać dodatkowy komunikat o tym jaką figurę wypisujesz.


import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ArrayList<Figure> lista= new ArrayList<>();
        Figure figura1 = new Kolo(5);
        Figure figura2= new Prostokat(5, 4);
        Figure figura3= new Kwadrat(5);
        lista.add(figura1);
        lista.add(figura2);
        lista.add(figura3);
        for (Figure figurka: lista)
             {
                 System.out.println(figurka.checkFigure());
                 System.out.println(figurka.obliczPole());
                 System.out.println(figurka.obliczObwod());
        }


    }
}
