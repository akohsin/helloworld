package Java.zadanie20;

public class Kolo extends Figure{

    private int promien;

    public Kolo(int promien) {
        this.promien = promien;
    }


    @Override
    public double obliczObwod() {
        obwod=2*Math.PI*promien;
        return obwod;
    }

    @Override
    public double obliczPole() {
        pole=Math.PI*promien*promien;
        return pole;
    }



}