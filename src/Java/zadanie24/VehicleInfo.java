package Java.zadanie24;
//Zadanie 24:
//        Stwórz aplikację Autodstrada. Celem zadania będzie implementacja systemu autostrady.
//        Do tej implementacji będzie nam potrzebny klasa Highway która reprezentuje autostradę,
//        oraz klasa VehicleInfo która reprezentuje zbiór informacji o pojeździe - tam będziemy przechowywać
//        takie dane jak jego rejestracja, jego typ(ciezarowka czy osobowy) i datę i godzinę wjazdu
//        - po której będziemy rozliczać pojazdy wyjezdzające z autostrady.

//        1. Stwórz enum CarType, który posiada typy TRUCK, CAR, MOTORCYCLE.
//        2. Stwórz klasę VehicleInfo, która powinna posiadać numer rejestracyjny pojazdu (String),
//        typ pojazdu (CarType), oraz datę WJAZDU na autostradę.
//        3. Stwórz klasę Highway która będzie posiadać :
//        - kolekcję wszystkich pojazdów
//        - metodę vehicleEntry(String numer_rejestracyjny, oraz CarType type) - która dodaje do
//            kolekcji nową instancję VehicleInfo oraz wypisuje do konsoli komunikat. Metoda nic nie zwraca.
//        - metodę searchVehicle(String numer_rejestracyjny) - która szuka pojazdu i wypisuje jego
//        informacje jeśli wciąż znajduje się na autostradzie
//        - metodę wyjazdu - vehicleLeave(String numer_rejestracyjny) - która usuwa samochód z kolekcji,
//        wypisuje komunikat, oraz na podstawie czasu jaki samochód znajdował się na autostradzie oblicza
//        jej kwotę do zapłaty i wypisuje ją do konsoli.
//        4. Stwórz main który przyjmuje komendy
//        - wjazd NR_REJESTRACYJNY TYP
//        - wyjazd NR_REJESTRACYJNY
//        - sprawdz NR_REJESTRACYJNY
//        i wykonuje odpowiednie akcje na instancji klasy highway.

import java.sql.Timestamp;

public class VehicleInfo {
    String numerRejestracyjny;
    Enum<CarType> carTypeEnum;
    Timestamp dataWjazdu;

    public String getNumerRejestracyjny() {
        return numerRejestracyjny;
    }

    public void setNumerRejestracyjny(String numerRejestracyjny) {
        this.numerRejestracyjny = numerRejestracyjny;
    }

    public Enum<CarType> getCarTypeEnum() {
        return carTypeEnum;
    }

    public void setCarTypeEnum(Enum<CarType> carTypeEnum) {
        this.carTypeEnum = carTypeEnum;
    }

    public Timestamp getDataWjazdu() {
        return dataWjazdu;
    }

    public void setDataWjazdu(Timestamp dataWjazdu) {
        this.dataWjazdu = dataWjazdu;
    }

    public VehicleInfo(String numerRejestracyjny, Enum<CarType> carTypeEnum) {

        this.numerRejestracyjny = numerRejestracyjny;
        this.carTypeEnum = carTypeEnum;
    }

    @Override
    public String toString() {
        return "VehicleInfo{" +
                "numerRejestracyjny='" + numerRejestracyjny + '\'' +
                ", carTypeEnum=" + carTypeEnum +
                ", dataWjazdu=" + dataWjazdu +
                '}';
    }
}
