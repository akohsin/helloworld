package Java.zadanie24;
//Zadanie 24:
//        Stwórz aplikację Autodstrada. Celem zadania będzie implementacja systemu autostrady.
//        Do tej implementacji będzie nam potrzebny klasa Highway która reprezentuje autostradę,
//        oraz klasa VehicleInfo która reprezentuje zbiór informacji o pojeździe - tam będziemy przechowywać
//        takie dane jak jego rejestracja, jego typ(ciezarowka czy osobowy) i datę i godzinę wjazdu
//        - po której będziemy rozliczać pojazdy wyjezdzające z autostrady.
//        1. Stwórz enum CarType, który posiada typy TRUCK, CAR, MOTORCYCLE.
//        2. Stwórz klasę VehicleInfo, która powinna posiadać numer rejestracyjny pojazdu (String),
//        typ pojazdu (CarType), oraz datę WJAZDU na autostradę.
//        3. Stwórz klasę Highway która będzie posiadać :
//        - kolekcję wszystkich pojazdów
//        - metodę vehicleEntry(String numer_rejestracyjny, oraz CarType type) - która dodaje do
//            kolekcji nową instancję VehicleInfo oraz wypisuje do konsoli komunikat. Metoda nic nie zwraca.
//        - metodę searchVehicle(String numer_rejestracyjny) - która szuka pojazdu i wypisuje jego
//        informacje jeśli wciąż znajduje się na autostradzie
//        - metodę wyjazdu - vehicleLeave(String numer_rejestracyjny) - która usuwa samochód z kolekcji,
//        wypisuje komunikat, oraz na podstawie czasu jaki samochód znajdował się na autostradzie oblicza
//        jej kwotę do zapłaty i wypisuje ją do konsoli.
//        4. Stwórz main który przyjmuje komendy
//        - wjazd NR_REJESTRACYJNY TYP
//        - wyjazd NR_REJESTRACYJNY
//        - sprawdz NR_REJESTRACYJNY
//        i wykonuje odpowiednie akcje na instancji klasy highway.


import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.HashMap;

public class Highway {
    private HashMap<String, VehicleInfo> kolekcjaSamochodow = new HashMap<>();

    public void vehicleLeave(String numerRejestracyjny){
        System.out.println("autostrade opuszcza" + kolekcjaSamochodow.get(numerRejestracyjny).toString());

        LocalDateTime czasWyjazd = LocalDateTime.now();
        Timestamp tWyjazd = Timestamp.valueOf(czasWyjazd);
        Long roznicaT = tWyjazd.getTime()+5 - kolekcjaSamochodow.get(numerRejestracyjny).getDataWjazdu().getTime();
        System.out.println("kwota do zaplaty: "+ roznicaT*4);
        kolekcjaSamochodow.remove(numerRejestracyjny);

    }

    public void vehicleEntry(String numerRejestracyjny, CarType type) {
        kolekcjaSamochodow.put(numerRejestracyjny, new VehicleInfo(numerRejestracyjny, type));

        LocalDateTime czasWjazd = LocalDateTime.now();
        Timestamp tWjazd = Timestamp.valueOf(czasWjazd);
        kolekcjaSamochodow.get(numerRejestracyjny).setDataWjazdu(tWjazd);

        System.out.println("na autostradę wjechał/a: " + type + " o numerze rejestracyjnym " + numerRejestracyjny);
    }

    public void vehicleEntry(VehicleInfo pojazd) {
        kolekcjaSamochodow.put(pojazd.getNumerRejestracyjny(), pojazd);
        LocalDateTime czasWjazd = LocalDateTime.now();
        Timestamp tWjazd = Timestamp.valueOf(czasWjazd);
        pojazd.setDataWjazdu(tWjazd);

        System.out.println("na autostradę wjechał/a: " + pojazd.getCarTypeEnum() + " o numerze rejestracyjnym " + pojazd.getNumerRejestracyjny());
    }

    public void searchVehicle(String numerRejestracyjny) {
        if (kolekcjaSamochodow.containsKey(numerRejestracyjny))
                System.out.println(kolekcjaSamochodow.get(numerRejestracyjny.toString()));
            }


    }


